package codes.probst.docsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import codes.probst.docsearch.interceptor.ResourceNotFoundHandlerInterceptor;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@Import({ ApplicationConfiguration.class, I18nConfiguration.class })
public class Application extends WebMvcConfigurerAdapter {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public HandlerInterceptor resourceNotFoundHandlerInterceptor() {
		return new ResourceNotFoundHandlerInterceptor();
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(resourceNotFoundHandlerInterceptor());
	}
}
