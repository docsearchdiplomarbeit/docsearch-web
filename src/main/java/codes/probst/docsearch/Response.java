package codes.probst.docsearch;

import java.util.List;

public class Response<T> {
	private T data;
	private List<String> errors;
	private List<String> messages;

	public Response(T data, List<String> errors, List<String> messages) {
		this.data = data;
		this.errors = errors;
		this.messages = messages;
	}

	public T getData() {
		return data;
	}

	public List<String> getErrors() {
		return errors;
	}

	public List<String> getMessages() {
		return messages;
	}

}