package codes.probst.docsearch.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import codes.probst.docsearch.ResourceNotFoundException;
import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.facade.ClassFacade;
import codes.probst.docsearch.clazz.facade.ClassPopulationOption;

@Controller
@RequestMapping("/projects/*/{project-id}/*/{version-id}/*/{package-id}")
public class ClassPageController {
	@Autowired
	private ClassFacade classFacade;
	
	@RequestMapping("/*/{class-id}")
	public String detail(@PathVariable("class-id") Long classId, Model model) {
		Optional<ClassData> optional = classFacade.getByKey(classId,
			ClassPopulationOption.OPTION_PROJECT,
			ClassPopulationOption.OPTION_VERSION,
			ClassPopulationOption.OPTION_PACKAGE,
			ClassPopulationOption.OPTION_DESCRIPTION,
			ClassPopulationOption.OPTION_METHODS,
			ClassPopulationOption.OPTION_SOURCE
		);
		
		if(optional.isPresent()) {
			model.addAttribute("clazz", optional.get());
		} else {
			throw new ResourceNotFoundException();
		}
		
		return "clazz/detail";
	}
}
