package codes.probst.docsearch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import codes.probst.docsearch.ResourceNotFoundException;

@ControllerAdvice
public class ExceptionHandlingController {
	@Autowired
	private MessageSource messageSource;

	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Throwable.class)
	public String generic(Model model) {
		fillModel(model, "500");
		return "exception/detail";
	}
	
	@ResponseStatus(value=HttpStatus.NOT_FOUND)
	@ExceptionHandler(value = {ResourceNotFoundException.class})
	public String notFound(Model model) {
		fillModel(model, "404");
		return "exception/detail";
	}
	
	private void fillModel(Model model, String statusCode) {
		model.addAttribute("statusCode", statusCode);
		model.addAttribute("statusMessage", messageSource.getMessage("exceptionhandlingcontroller." + statusCode, new Object[0], LocaleContextHolder.getLocale()));
	}
}
