package codes.probst.docsearch.controller;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import codes.probst.docsearch.Response;
import codes.probst.docsearch.queue.facade.QueueEntryFacade;

@Controller
@RequestMapping("/import")
public class ImportController {
	private static final Logger LOG = LoggerFactory.getLogger(ImportController.class);
	
	@Autowired
	private QueueEntryFacade queueEntryFacade;;
	@Autowired
	private MessageSource messages;
	
	@RequestMapping(method = RequestMethod.GET) 
	public String html() {
		return "import";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> upload(@RequestParam("compiled") MultipartFile compiled, @RequestParam("source") MultipartFile source) {
		try {
			queueEntryFacade.importEntry(compiled.getBytes(), source.getBytes());
		} catch (IOException e) {
			LOG.error("Could not upload files", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
		
		String message = messages.getMessage("importcontroller.import.success", new Object[0], LocaleContextHolder.getLocale());
		return ResponseEntity.ok().body(new Response<Object>(null, null, Arrays.asList(message)));
	}
	

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public ResponseEntity<?> illegalArgumentException(HttpServletResponse response, IllegalArgumentException e) {
		String message = messages.getMessage("importcontroller.import." + e.getMessage(), new Object[0], LocaleContextHolder.getLocale());
		return ResponseEntity.badRequest().body(new Response<Object>(null, Arrays.asList(message), null));
	}

}
