package codes.probst.docsearch.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import codes.probst.docsearch.ResourceNotFoundException;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.facade.PackageFacade;
import codes.probst.docsearch.packages.facade.PackagePopulationOption;

@Controller
@RequestMapping("/projects/*/{project-id}/*/{version-id}")
public class PackagePageController {
	@Autowired
	private PackageFacade packageFacade;
	
	@RequestMapping("/*/{package-id}")
	public String detail(@PathVariable("package-id") Long packageId, Model model) {
		Optional<PackageData> optional = packageFacade.getByKey(packageId,
			PackagePopulationOption.OPTION_DOCUMENTATION,
			PackagePopulationOption.OPTION_VERSION,
			PackagePopulationOption.OPTION_CLASSES
		);
		
		if(optional.isPresent()) {
			model.addAttribute("packagez", optional.get());
		} else {
			throw new ResourceNotFoundException();
		}
		
		return "packagez/detail";
	}
}
