package codes.probst.docsearch.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import codes.probst.docsearch.ResourceNotFoundException;
import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.facade.ProjectFacade;
import codes.probst.docsearch.project.facade.ProjectPopulationOption;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

@Controller
@RequestMapping("/projects")
public class ProjectsPageController {
	@Autowired
	private ProjectFacade projectFacade;
	
	@RequestMapping
	public String overview(Model model) {
		Map<String, Collection<ProjectData>> projects = new TreeMap<>(String::compareTo);
		
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] {
			new Sort("artifactId", SortDirection.ASCENDING)
		});
		
		projectFacade.getAll(pageable, ProjectPopulationOption.OPTION_URL).forEach(project -> {
			String firstCharacter = String.valueOf(Character.toUpperCase(project.getArtifactId().charAt(0)));
			
			Collection<ProjectData> projectsPerCharacter = projects.get(firstCharacter);
			if(projectsPerCharacter == null) {
				projectsPerCharacter = new ArrayList<>();
				projects.put(firstCharacter, projectsPerCharacter);
			}
			projectsPerCharacter.add(project);
		});
		
		model.addAttribute("projects", projects);
		
		return "projects/overview";
	}
	
	@RequestMapping(value = "/*/{id}", method = RequestMethod.GET)
	public String detail(@PathVariable("id") Long id, Model model) {
		Optional<ProjectData> project = projectFacade.getByKey(id, ProjectPopulationOption.OPTION_VERSIONS);
		if(project.isPresent()) { 
			model.addAttribute("project", project.get());
		} else {
			throw new ResourceNotFoundException();
		}
		
		return "projects/detail";
	}
}
