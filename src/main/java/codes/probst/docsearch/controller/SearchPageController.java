package codes.probst.docsearch.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import codes.probst.docsearch.search.facade.SearchFacade;
import codes.probst.docsearch.search.facade.SearchResultData;
import codes.probst.framework.pagination.Pageable;

@Controller
@RequestMapping("/search")
public class SearchPageController {

	@Autowired
	private SearchFacade searchFacade;

	@RequestMapping(params = "q")
	public String search(@RequestParam("q") String searchQuery, HttpServletRequest request, Model model) {
		Pageable pageable = new Pageable();
		pageable.setCurrentPage(0);
		pageable.setPageSize(21);
		
		Map<String, Map<String, String>> appliedFilters = new HashMap<>();
		for(Enumeration<String> namesEnum = request.getParameterNames(); namesEnum.hasMoreElements();) {
			String name = namesEnum.nextElement();
			
			String mappedName = mapToName(name);
			if(mappedName != null) {
				Map<String, String> filters = appliedFilters.get(mappedName);
				if(filters == null) {
					filters = new HashMap<>();
					appliedFilters.put(mappedName, filters);
				}
				filters.put(name.substring(name.indexOf("_") + 1), request.getParameter(name));
			}
		}
		
		SearchResultData result = searchFacade.search(searchQuery, appliedFilters, pageable);
		model.addAttribute("searchResult", result);
		
		return "search/overview";
	}
	
	private String mapToName(String key) {
		if(key.startsWith("class_")) {
			return "class";
		} else if(key.startsWith("package_")) {
			return "package";
		} else if(key.startsWith("project_")) {
			return "project";
		}
		return null;
	}
}
