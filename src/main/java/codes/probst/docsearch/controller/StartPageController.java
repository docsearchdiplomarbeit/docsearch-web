package codes.probst.docsearch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import codes.probst.docsearch.search.facade.SearchFacade;
import codes.probst.framework.pagination.Pageable;

@Controller
public class StartPageController {
	@Autowired
	private SearchFacade searchFacade;
	
	@RequestMapping("/")
	public String startpage(Model model) {
		Pageable pageable = new Pageable();
		pageable.setCurrentPage(0);
		pageable.setPageSize(6);
		
		model.addAttribute("recommendations", searchFacade.getRandomClasses(pageable));
		
		return "startpage";
	}
}
