package codes.probst.docsearch.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import codes.probst.docsearch.ResourceNotFoundException;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.facade.VersionFacade;
import codes.probst.docsearch.version.facade.VersionPopulationOption;

@Controller
@RequestMapping("/projects/*/{project-id}/")
public class VersionPageController {
	@Autowired
	private VersionFacade versionFacade;
	
	@RequestMapping("/*/{version-id}")
	public String detail(@PathVariable("version-id") Long versionId, Model model) {
		Optional<VersionData> optional = versionFacade.getByKey(versionId,
			VersionPopulationOption.OPTION_URL,
			VersionPopulationOption.OPTION_PACKAGES,
			VersionPopulationOption.OPTION_PROJECT
		);
		
		if(optional.isPresent()) {
			model.addAttribute("version", optional.get());
		} else {
			throw new ResourceNotFoundException();
		}
		
		
		return "version/detail";
	}

}
